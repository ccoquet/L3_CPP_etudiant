//
// Created by ccoquet on 29/03/19.
//

#ifndef PROJECT_IMAGE_HPP
#define PROJECT_IMAGE_HPP

#include <string>

class Image {

private:
    int _largeur;
    int _hauteur;
    int* _pixels;

public:
    Image(int largeur, int hauteur);
    ~Image();
    int getLargeur() const;
    int getHauteur() const;
    int getPixel(int i, int j) const;
    void setPixel(int i, int j, int couleur);

};

void ecrirePnm(const Image & img,const std::string & nomFichier);

#endif //PROJECT_IMAGE_HPP
