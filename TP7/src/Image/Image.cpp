//
// Created by ccoquet on 29/03/19.
//

#include <iostream>
#include <fstream>
#include <string>
#include "Image.hpp"

Image::Image(int largeur, int hauteur) : _largeur(largeur), _hauteur(hauteur){
    _pixels=new int [(_largeur *_hauteur)];
}

Image::~Image() {
    delete[] _pixels;
 }

int Image::getLargeur() const{
    return _largeur;
}
int Image::getHauteur() const{
    return _hauteur;
}
int Image::getPixel(int i, int j) const{
    if (i<0 || i>_largeur-1 ||  j<0 || j>_hauteur-1){//si je suis hors cadre message d'erreur
        throw std::string("erreur parametres");
    }
    return _pixels[i+((_largeur-1)*j)];
}

void Image::setPixel(int i, int j, int couleur) {
    if (i < 0 || i > _largeur - 1 || j < 0 || j > _hauteur - 1) {//si je suis hors cadre message d'erreur
        throw std::string("coordonnées invalides");
    } else { //j'affecte une couleur au pixel situé en (i,j)
        _pixels[i+((_largeur-1)*j)] = couleur;
    }
}

void ecrirePnm(const Image & img, const std::string & nomFichier){

    std::fstream monFlux(nomFichier, std::ios::out);// flux de création ecriture

    if(monFlux.is_open()) {
        monFlux<<"P2"<<std::endl; //signature du fichier PNM
        monFlux<<img.getLargeur()<<" "<<img.getHauteur()<<std::endl; //dimensions de l'image
        monFlux<<15<<std::endl; //valeur max pour chaque couleur

        // je donne au pixel de l'image la val en RGB
        for( int y=0; y<img.getHauteur(); y++){
            for (int x=0; x<img.getLargeur(); x++){
                monFlux<<img.getPixel(x,y);
            }
            monFlux<<std::endl;
        }
        monFlux.close();
        return ;
    }
    else{
        throw std::string("erreur de creation");
    }
}


