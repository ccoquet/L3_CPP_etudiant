//
// Created by ccoquet on 29/03/19.
//
#include "Image/Image.hpp"
#include <iostream>

int main(){

    Image i1(5,5);
    //std::cout<<"largeur de l'image :"<<i1.getLargeur()<<std::endl;
    //std::cout<<"hauteur de l'image :"<<i1.getHauteur()<<std::endl;

    i1.setPixel(0,0,1);
    i1.setPixel(0,1,2);
    i1.setPixel(0,2,3);
    i1.setPixel(0,3,4);
    i1.setPixel(0,4,5);

    i1.setPixel(1,0,1);
    i1.setPixel(1,1,2);
    i1.setPixel(1,2,3);
    i1.setPixel(1,3,4);
    i1.setPixel(1,4,5);

    //std::cout<<i1.getPixel(1,1)<<std::endl;
    //std::cout<<i1.getPixel(1,4)<<std::endl;

    std::string file = "essaiImage.pnm";
    ecrirePnm(i1,file);
    return 0;
};