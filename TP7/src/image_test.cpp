//
// Created by ccoquet on 29/03/19.
//

#include "Image/Image.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupImage) { };

TEST(GroupImage, Image_constructeur){

    Image im(200,100);
    CHECK_EQUAL(im.getLargeur(),200);
    CHECK_EQUAL(im.getHauteur(),100);

}

TEST(GroupImage, Image_pixels_1){

    Image im(200,100);
    try {
        im.getPixel(180,80);
        FAIL( "OK 1" );
    }
    catch (const std::string& str) {
        CHECK_EQUAL(str,"erreur parametres");
    }
}

TEST(GroupImage, Image_pixels_2){

    Image im(200,100);
    try {
        im.getPixel(210,80);
        FAIL( "OK 2" );
    }
    catch (const std::string& str){
        CHECK_EQUAL(str,"erreur parametres");
    }
}

TEST(GroupImage, Image_pixels_3){

    Image im(200,100);
    try {
        im.setPixel(100,80,0);
        FAIL( "OK 3" );
    }
    catch (const std::string& str){
        CHECK_EQUAL(str,"coordonnées invalides");
    }
}


TEST(GroupImage, Image_pixels_4){

    Image im(200,100);
    try {
        im.setPixel(210,180,0);
        FAIL( "OK 4" );
    }
    catch (const std::string& str){
        CHECK_EQUAL(str,"coordonnées invalides");
    }
}

