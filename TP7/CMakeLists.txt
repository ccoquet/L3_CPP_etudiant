cmake_minimum_required( VERSION 3.0 )
project( TP7 ) #nom du projet
set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14 -Wall -Wextra" ) #activation des warning

find_package( PkgConfig REQUIRED ) # ces 3 lignes permettent au système de rechercher et de récupérer les fichiers souhaités (.hpp)
pkg_check_modules( PKG_CPPUTEST REQUIRED cpputest ) #vais chercher mes paquets
include_directories( ${PKG_CPPUTEST_INCLUDE_DIRS} ) #include tous les chemins des fichiers dont on a besoin

# programme principal (permet de dire ce que je veux générer - ici un éxécutable .out à l'aide des .cpp cités derrière)
add_executable( main_image.out src/main_image.cpp
        src/Image/Image.cpp src/Image/Image.hpp
        )
target_link_libraries( main_image.out ) #librairie à ajouter

# programme de test
add_executable( main_test.out src/main_test.cpp
        src/Image/Image.cpp src/Image/Image.hpp
        src/image_test.cpp
)
target_link_libraries( main_test.out ${PKG_CPPUTEST_LIBRARIES} )
