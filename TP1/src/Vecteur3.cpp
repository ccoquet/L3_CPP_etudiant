#include "Vecteur3.hpp"

#include <iostream>
#include <cmath>


Vecteur3::Vecteur3(float x1, float y1, float z1){
	x=x1;
	y=y1;
	z=z1;
}

Vecteur3::~Vecteur3(){ }

void Vecteur3::afficher(Vecteur3 v){
	std::cout<<"( "<<v.x<<" , "<<v.y<<" , "<<v.z<<" )"<<std::endl;
}


void Vecteur3::afficher(){
	std::cout<<"( "<<x<<" , "<<y<<" , "<<z<<" )"<<std::endl;
}

float Vecteur3::norme(Vecteur3 v){
	return (sqrt(pow(v.x,2)+pow(v.y,2)+pow(v.z,2)));
}

float Vecteur3::produitScalaire(Vecteur3 v){
	return (x*v.x + y*v.y + z*v.z);
}

void Vecteur3::addition(Vecteur3 v){
	std::cout<<"( "<<x+v.x<<" , "<<y+v.y<<" , "<<z+v.z<<" )"<<std::endl;
}