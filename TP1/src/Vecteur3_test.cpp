#include "Vecteur3.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupVecteur) { };

TEST(GroupVecteur,normeVecteur)  {
	Vecteur3 v(2.f,3.f,6.f);
    CHECK_EQUAL(v.norme(v),7);    
}

TEST(GroupVecteur,produitScalaireVecteur)  {
	Vecteur3 v1(2.f,3.f,6.f);
	Vecteur3 v2(3.f,4.f,5.f);
    CHECK_EQUAL(v1.produitScalaire(v1),49); 
    CHECK_EQUAL(v1.produitScalaire(v2),48); 
    CHECK_EQUAL(v2.produitScalaire(v2),50);
}
