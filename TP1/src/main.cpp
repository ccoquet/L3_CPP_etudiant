#include <iostream>
#include "fibonacci.hpp"
#include "Vecteur3.hpp"

int main(){

	/**premier programme */
	//std::cout<<"Premier Tp de C++"<<std::endl;	
	
	/** premier module */
	/**std::cout<<"fibo récursif"<<std::endl;
	for (int i=1; i<=7; i++){
		std::cout<<" "<<fibonacciRecursif(i)<<std::endl;		
	}

	std::cout<<"fibo itératif"<<std::endl;
	for (int i=1; i<=7; i++){
		std::cout<<" "<<fibonacciIteratif(i)<<std::endl;		
	}*/
	
	/**structure*/
	//creation du vecteur(2,3,6)
	Vecteur3 v1(2.f,3.f,6.f);
	std::cout<<"affichage avec passage en paramètres du vecteur :"<<std::endl;
	v1.afficher(v1);
	std::cout<<"affichage sans paramètres :"<<std::endl;
	v1.afficher();
	std::cout<<"affichage de la norme du vecteur :"<<std::endl;
	std::cout<< v1.norme(v1)<<std::endl;
	Vecteur3 v2(3.f,4.f,5.f);
	std::cout<<"affichage du produit scalaire des 2 vecteurs suivants:"<<std::endl;
	v1.afficher();
	std::cout<<" et "<<std::endl;
	v2.afficher();
	std::cout<<"résultat : "<< v1.produitScalaire(v2)<<std::endl;
	std::cout<<"affichage de l'addition des 2 vecteurs suivants:"<<std::endl;
	v1.afficher();
	std::cout<<" et "<<std::endl;
	v2.afficher();
	std::cout<<"résultat :"<<std::endl;
	v1.addition(v2);
	return 0;

}