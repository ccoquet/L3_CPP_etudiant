#ifndef VECTEUR3_HPP
#define VECTEUR3_HPP

class Vecteur3{

	private: //attributs
		float x;
		float y;
		float z;

	public : 
		//constructeur
		Vecteur3(float x1, float y1, float z1);

		//destructeur
		~Vecteur3();


	public : //méthodes
		//fonction d'affichage
		void afficher(Vecteur3 v);
		void afficher();
		float norme(Vecteur3 v);
		float produitScalaire(Vecteur3 v);
		void addition(Vecteur3 v);
};

#endif 