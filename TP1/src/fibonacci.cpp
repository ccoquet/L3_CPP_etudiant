#include "fibonacci.hpp"

#include <iostream>


int fibonacciRecursif(int n){
	if (n<2){
		return n;
	}
return fibonacciRecursif(n-1)+fibonacciRecursif(n-2);
}
	
int fibonacciIteratif(int n){
	int Fn1=1;
	int Fn2=1;
	int Fn=1;
	//initialisation de la formule de calcul
	for (int i=3; i<=n; i++){
		Fn=Fn1 + Fn2;
		Fn2=Fn1;
		Fn1=Fn;
	}
return Fn;
}

