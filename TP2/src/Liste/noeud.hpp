#ifndef NOEUD_HPP
#define NOEUD_HPP

struct Noeud {

    int _valeur;
    Noeud *_suivant;

    Noeud();

};
#endif // NOEUD_HPP
