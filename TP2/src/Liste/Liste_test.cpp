#include "liste.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupListe) { };

TEST(GroupListe, Liste_test1)  {
    Liste l;
    l.ajouterDevant(4);
    l.ajouterDevant(8);
    CHECK_EQUAL(l.getTaille(),2);
}

TEST(GroupListe, Liste_test2)  {
    Liste l;
    l.ajouterDevant(4);
    l.ajouterDevant(8);
    l.ajouterDevant(12);
    CHECK_EQUAL(l.getElement(1),8);
}

