#ifndef LISTE_HPP
#define LISTE_HPP

#include "noeud.hpp"

struct Liste {

    Noeud *_tete;

   Liste();
   ~Liste();
   void ajouterDevant(int valeur);
   int getTaille() const;
   int getElement(int indice) const;
};

#endif // LISTE_HPP
