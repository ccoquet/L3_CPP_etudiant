#include "liste.hpp"
#include <iostream>

Liste::Liste():_tete(nullptr){}

Liste::~Liste() {
    Noeud *cour;// pointeur vers le maillon à effacer
    while(_tete){// parcours de la liste
        cour = _tete;
        _tete = _tete->_suivant;
        delete cour;
    }
}

void Liste::ajouterDevant(int valeur){
    Noeud *nouveau= new Noeud();
    nouveau->_valeur=valeur;
    nouveau->_suivant=_tete;
    _tete=nouveau;
}

int Liste::getTaille() const{
    int taille =0;
    Noeud * courant= _tete;
    while (courant !=nullptr) {
        taille++;
        courant=courant->_suivant;
    }
    return taille;
}


int Liste::getElement(int indice) const {
    Noeud * courant= _tete;
    int element=0;   
    if (indice > getTaille()){
        std::cout<<"Indice superieur à la taille du tableau"<<std::endl;
    }     
    for (int i=0; i<indice;i++){
        courant=courant->_suivant;
    }
    element=courant->_valeur;
    return element;
}

