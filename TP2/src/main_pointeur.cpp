#include "Liste/noeud.hpp"
#include "Liste/liste.hpp"

#include <iostream>

int main() {
    //partie 1 TP2
    /*int a=42;
    int *p;
    p= &a;
    std::cout<<a<<std::endl;
    *p=37;
    std::cout<<a<<std::endl;

    //partie 2 TP2
    int *t = new int[10];
    *(t+2)=42;
    std::cout<<t[2]<<std::endl;
    delete [] t;
    t=nullptr;*/

    //implementation liste chainee et test des différents méthodes
    /*Liste l;
    int t=0;
    int valIndice=0;
    l.ajouterDevant(5);
    l.ajouterDevant(8);
    l.ajouterDevant(4);
    t=l.getTaille();
    std::cout<<"la taille de mon tableau est :"<<t<<std::endl;
    valIndice=l.getElement(2);
    std::cout<<"l'element à l'indice souhaite est :"<<valIndice<<std::endl;*/

    //programme pour analyse des fuites de mémoire
    Liste l;
    l.ajouterDevant(37);
    l.ajouterDevant(13);
    for (int i=0; i<l.getTaille();i++){
        std::cout<<l.getElement(i)<<std::endl;
    }
  return 0;
}

