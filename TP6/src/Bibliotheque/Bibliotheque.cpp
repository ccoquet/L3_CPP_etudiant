//
// Created by ccoquet on 29/03/19.
//

#include "Bibliotheque.hpp"

#include <iostream>
#include <algorithm>
#include <fstream>


Bibliotheque::Bibliotheque(){}


void  Bibliotheque::afficher() const{
        for (auto it=begin(); it!=end(); it++) {
                std::cout << *it<<std::endl;
        }
}

void Bibliotheque::trierParAuteurEtTitre(){
    std::sort(begin(), end(), [](Livre l1, Livre l2) {
        return l1 < l2;
    });
}

void Bibliotheque::trierParAnnee(){
    std::sort(begin(), end(), [](Livre l1, Livre l2) {
        return l1.getAnnee() < l2.getAnnee();
    });
}

void Bibliotheque::lireFichier(const std::string& nomFichier){
    std::fstream fluxE(nomFichier, std::ios::in); //ouverture en lecture
    if(fluxE.good()){ //si l'ouverture du fichier a fonctionné
        Livre l; //pour recuperer le descriptif de chaque livre
        fluxE>>l; //je place la 1ere ligne de mon fichier dans l
        while(!fluxE.eof()){ //tant que je ne suis pas à la fin de mon fichier
            push_back(l); //j'ajoute le livre recuperé, à ma bibliothèque
            fluxE >> l;
        }
        fluxE.close();
    }else{
        throw std::string("erreur : lecture du fichier impossible");
    }
    //fluxE.close();
}

void Bibliotheque::ecrireFichier(const std::string& nomFichier){
    std::fstream fluxS(nomFichier, std::ios::out); //ouverture en écriture
    if(fluxS.good()){ //si l'ouverture du fichier a fonctionné
        //je parcours ma bibliothèque et en envoie le contenu dans mon fichier
        for (auto it=this->begin(); it !=this->end(); ++it){
            fluxS<< (*it) <<std::endl;
        }
    fluxS.close();
    }else{
        throw std::string("erreur : ecriture dans fichier impossible");
    }
    //fluxS.close();
}