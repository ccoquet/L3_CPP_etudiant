//
// Created by ccoquet on 29/03/19.
//

#ifndef TP6_BIBLIOTHEQUE_HPP
#define TP6_BIBLIOTHEQUE_HPP

#include "../Livre/Livre.hpp"
#include <string>

class Bibliotheque : public std::vector<Livre> {

public:
    Bibliotheque();
    void  afficher() const;
    void trierParAuteurEtTitre();
    void trierParAnnee();
    void lireFichier(const std::string& nomFichier);
    void  ecrireFichier(const std::string& nomFichier);

};


#endif //TP6_BIBLIOTHEQUE_HPP
