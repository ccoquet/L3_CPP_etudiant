//
// Created by ccoquet on 22/03/19.
//

#ifndef TP6_LIVRE_HPP
#define TP6_LIVRE_HPP

#include <string>
#include <iostream>
#include <regex>


class Livre {

private:
    std::string _titre;
    std::string _auteur;
    int _annee;

public:
    Livre();
    Livre(const std::string& titre, const std::string& auteur, int annee);
    const std::string& getTitre() const;
    const std::string& getAuteur() const;
    int getAnnee() const;
    bool operator<(const Livre& l2) const;
    friend std::istream& operator>>(std::istream& in, Livre& l2);

};

bool operator==(const Livre& l1, const Livre& l2);

std::ostream& operator<<(std::ostream& out, const Livre& l2);





#endif //TP6_LIVRE_HPP
