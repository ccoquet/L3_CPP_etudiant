//
// Created by ccoquet on 22/03/19.
//

#include "Livre.hpp"

Livre::Livre() {};

Livre::Livre(const std::string &titre, const std::string &auteur, int annee) : _titre(titre), _auteur(auteur),
                                                                               _annee(annee) {
    std::string pV(";");
    std::string rC("\n");
    std::size_t pVTFound = getTitre().find(pV);
    std::size_t retLTFound = getTitre().find(rC);
    std::size_t pVAFound = getAuteur().find(pV);
    std::size_t retLAFound = getAuteur().find(rC);


    if (pVTFound != std::string::npos) {
        throw std::string("erreur : titre non valide (';' non autorisé)");
    }
    if (retLTFound != std::string::npos) {
        throw std::string("erreur : titre non valide (';' non autorisé)");
    }
    if (pVAFound != std::string::npos) {
        throw std::string("erreur : auteur non valide (';' non autorisé)");
    }
    if (retLAFound != std::string::npos) {
        throw std::string("erreur : auteur non valide ('\n' non autorisé)");
    }
}

const std::string& Livre::getTitre() const {
    return _titre;
}

const std::string& Livre::getAuteur() const {
    return _auteur;
}

int Livre::getAnnee() const {
    return _annee;
}

bool Livre::operator<(const Livre& l2) const{
    if(_auteur< l2.getAuteur()){
        return true;
    }else if (l2.getAuteur()<_auteur){
        return false;
    }
    if (_titre <l2.getTitre()){
        return true;
    }else if (l2.getTitre()<_titre){
        return false;
    }
return false;
}

bool operator==(const Livre& l1, const Livre& l2) {
    return l1.getTitre() == l2.getTitre() and l1.getAuteur() == l2.getAuteur()
           and l1.getAnnee() == l2.getAnnee();
}


/**implementation de l'operateur de flux de sortie */

std::ostream& operator<<(std::ostream& out, const Livre& l2){
    return out << l2.getTitre()<<";"<<l2.getAuteur()<<";"<<l2.getAnnee();
}

/**implementation de l'operateur de flux d'entree */

std::istream& operator>>(std::istream& in, Livre& l2){

    getline(in, l2._titre,';');
    getline(in, l2._auteur,';');
    //declaration en string car c'est le type qu'on recup avec getLine, hors annee est un int
    std::string sAnnee;
    getline(in,sAnnee,';');
    l2._annee =stoi(sAnnee); //modification d'un string en int

    return in;
}