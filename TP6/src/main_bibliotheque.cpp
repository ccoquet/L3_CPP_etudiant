#include "Bibliotheque/Bibliotheque.hpp"

#include <iostream>
#include <vector>


int main() {

    Livre l1("t1","a1",13);
    Livre l2("t2","a2",25);
    Livre l3("t3","a3",05);
    Bibliotheque b1;
    //remplissage de ma bibliotheque
    b1.push_back(l1);
    b1.push_back(l2);
    b1.push_back(l3);

    //std::cout<<b1[1]<<std::endl;
    //affichage du contenu de ma bibliotheque
    b1.afficher();
    b1.ecrireFichier("bibliotheque_fichier_tmp.txt");
    std::cout<<std::endl;

    Bibliotheque b2;
    b2.lireFichier("bibliotheque_fichier_tmp.txt");
    b2.afficher();
    return 0;
}

