#include "Controleur.hpp"


#include <cassert>
#include <fstream>
#include <iostream>
#include <sstream>

Controleur::Controleur(int argc, char ** argv) {
    //ajout des vues console et graphique
    _vues.push_back(std::make_unique<VueConsole>( *this));
    _vues.push_back(std::make_unique<VueGraphique>(argc, argv, *this));
    //ajout d'une bouteille de test
    //_inventaire._bouteilles.push_back(Bouteille{"Test","1978-06-07", 0.75f});
    //chargerInventaire("FichierT");
    //for (auto & v : _vues)
    //  v->actualiser();
}

void Controleur::run() {
    for (auto & v : _vues)
        v->run();
}

std::string Controleur::getTexte() const{
    std::ostringstream oss;
    oss<< _inventaire;
    return oss.str();
}

void Controleur::chargerInventaire(const std::string& nomFichier){
    //ajout d'une bouteille test
    _inventaire._bouteilles.push_back(Bouteille{"Test","1978-06-07", 0.75f});
    //mise à jour des vues
    for (auto & v : _vues)
        v->actualiser();

}
