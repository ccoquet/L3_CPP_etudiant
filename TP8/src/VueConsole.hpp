//
// Created by ccoquet on 05/04/19.
//

#ifndef TP8_VUECONSOLE_HPP
#define TP8_VUECONSOLE_HPP

#include "Vue.hpp"

class Controleur;

class VueConsole : public Vue{

public:
    VueConsole(Controleur & controleur);
    void actualiser();
    void run();

};


#endif //TP8_VUECONSOLE_HPP
