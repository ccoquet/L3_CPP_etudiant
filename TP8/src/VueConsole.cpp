//
// Created by ccoquet on 05/04/19.
//
#include "Controleur.hpp"
#include "VueConsole.hpp"

#include<iostream>

VueConsole::VueConsole(Controleur & controleur): Vue(controleur){}

void VueConsole::actualiser(){
    std::string texte = _controleur.getTexte();
    std::cout<< texte<<std::endl;
}

void VueConsole::run(){}