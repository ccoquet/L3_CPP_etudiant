#include "Inventaire.hpp"


std::ostream & operator<<(std::ostream & os, const Inventaire & i){
    std::locale vieuxLoc = std::locale::global(std::locale("fr_FR.UTF-8"));
    for(Bouteille b : i._bouteilles ){
        os<<b;
    }
    std::locale::global(vieuxLoc);
    return os;
}


