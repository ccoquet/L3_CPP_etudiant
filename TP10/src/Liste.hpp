
#ifndef LISTE_HPP_
#define LISTE_HPP_

#include <cassert>
#include <ostream>

// liste d'entiers avec itérateur
class Liste {
    private:

        struct Noeud {
            int _valeur;
            Noeud* _ptrNoeudSuivant;
        };

        Noeud* _ptrTete;

    public:
        class iterator {
            private:

                Noeud* _ptrNoeudCourant;

            public:
                iterator();

                iterator(Noeud* ptrNoeudCourant) : _ptrNoeudCourant(ptrNoeudCourant) {}

                const iterator & operator++() {
                    return *this;
                }

                int& operator*() const {
                    static int nimpe;
                    return nimpe;
                }

                bool operator!=(const iterator &) const {
                    return false;
                }

                friend Liste; 
        };

    public:

        Liste(){
            _ptrTete=nullptr;
        }

        ~Liste(){
            Noeud *cour;// pointeur vers le Noued à effacer
            while(_ptrTete){// parcourir la liste
                cour = _ptrTete;
                _ptrTete = _ptrTete->_ptrNoeudSuivant;
                delete cour;
            }
        }

        void push_front(int valeur) {
            _ptrTete->_valeur=valeur;
        }

        int& front() const {
            static int nimpe;
            return nimpe;
        }

        void clear() {
        }

        bool empty() const {
            return true;
        }

        iterator begin() const {
            return iterator();
        }

        iterator end() const {
            return iterator();
        }

};

std::ostream& operator<<(std::ostream& os, const Liste&) {
    return os;
}

#endif

