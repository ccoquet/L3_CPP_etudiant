#include "Magasin.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupMagasin){};

TEST(GroupMagasin, Magasin_test1)
{
    Magasin m;
    m.ajouterProduit("canape");
    CHECK_EQUAL(m.nbProduits(), 1);
}

TEST(GroupMagasin, Magasin_test2)
{
    Magasin m;
    m.ajouterClient("Léo");
    m.ajouterClient("Léa");
    CHECK_EQUAL(m.nbClients(), 2);
}

TEST(GroupMagasin, Magasin_test3)
{
    Magasin m;
    CHECK_THROWS(std::string ,m.supprimerClient(1));
}

TEST(GroupMagasin, Magasin_test4)
{
    Magasin m;
    m.ajouterProduit("chaise");
    m.ajouterProduit("table");
    CHECK_EQUAL(m.nbProduits(), 2);
}

TEST(GroupMagasin, Magasin_test5)
{
    Magasin m;
    CHECK_THROWS(std::string ,m.supprimerProduit(1));
}

TEST(GroupMagasin, Magasin_test6)
{
    Magasin m;
    m.ajouterLocation(10,2);
    m.ajouterLocation(11,3);
    m.ajouterLocation(12,4);
    CHECK_EQUAL(m.nbLocations(), 3);
}

TEST(GroupMagasin, Magasin_test7)
{
    Magasin m;
    CHECK_THROWS(std::string ,m.supprimerLocation(10,2));
}

TEST(GroupMagasin, Magasin_test8)
{
    Magasin m;
    m.ajouterLocation(1,3);
    m.ajouterLocation(0,0);
    m.ajouterLocation(3,1);
    CHECK_EQUAL(m.trouverClientDansLocation(2), false);
    CHECK_EQUAL(m.trouverClientDansLocation(1), true);
    CHECK_EQUAL(m.trouverProduitDansLocation(2), false);
    CHECK_EQUAL(m.trouverProduitDansLocation(0), true);
}