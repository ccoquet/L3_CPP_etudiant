//
// Created by ccoquet on 01/03/19.
//

#include "Magasin.hpp"

#include <iostream>
#include <algorithm>
#include <vector>

/**constructeur*/
Magasin::Magasin() : _idCourantClient(0), _idCourantProduit(0){}

/**méthodes */
int Magasin::nbClients() const{
    return _clients.size();
}

void Magasin::ajouterClient(const std::string & nom){
    _clients.push_back(Client(_idCourantClient, nom));
    _idCourantClient++;
}

void  Magasin::afficherClients() const{
    for(Client c: _clients){
        c.afficherClient();
    }
}

void Magasin::supprimerClient(int idClient){
    bool clientTrouve=false;
    for (int i=0; i<_clients.size(); i++){
        if(_clients[i].getId()==idClient){
            std::swap(_clients[i],_clients[_idCourantClient-1]);
            _clients.pop_back();
            _idCourantClient--;
            clientTrouve=true;
            break;
        }
    }
    if(!clientTrouve){
        throw std::string("Ce client n'existe pas");
    }
}

int Magasin::nbProduits() const{
    return _produits.size();
}

void Magasin::ajouterProduit(const std::string & nom){
    _produits.push_back(Produit(_idCourantProduit, nom));
    _idCourantProduit++;
}

void Magasin::afficherProduits() const{
    for (Produit p: _produits){
        p.afficherProduit();
    }
}

void Magasin::supprimerProduit(int idProduit){
    bool produitTrouve=false;
    for (int i=0; i<_produits.size(); i++){
        if (_produits[i].getId()==idProduit){
            std::swap(_produits[i],_produits[_idCourantProduit-1]);
            _produits.pop_back();
            _idCourantProduit--;
            produitTrouve=true;
            break;
        }
    }
    if(!produitTrouve){
        throw std::string("Ce produit n'existe pas");
    }
}

int Magasin::nbLocations() const{
    return _locations.size();
}

void Magasin::ajouterLocation(int idClient, int idProduit){
    _locations.push_back(Location(idClient,idProduit));
}

void Magasin::afficherLocation() const{
    for (Location l: _locations){
        l.afficherLocation();
    }
}

void Magasin::supprimerLocation(int idClient, int idProduit){
    bool produitTrouve=false;
    for (int i=0; i<nbLocations(); i++){
        if ((_locations[i]._idClient==idClient) && (_locations[i]._idProduit==idProduit)){
            std::swap(_locations[i],_locations[nbLocations()-1]);
            _locations.pop_back();
            produitTrouve=true;
            break;
        }
    }
    if(!produitTrouve){
        throw std::string("Cette location n'existe pas");
    }
}

bool Magasin::trouverClientDansLocation(int idClient) const{
    for (int i=0; i<_locations.size(); i++){
        if (_locations[i]._idClient==idClient){
            return true;
        }
    }
    return false;
}

std::vector<int> Magasin::calculerClientsLibres() const{
    std::vector<int> _tabClientsLibres;
    for (int i=0; i<_clients.size(); i++) {
        if (!trouverClientDansLocation(_clients[i].getId())) {
            _tabClientsLibres.push_back(_clients[i].getId());
        }
    }
    for (int i=0; i<_tabClientsLibres.size();i++){
        std::cout<<_tabClientsLibres[i]<<std::endl;
    }

return _tabClientsLibres;
}

bool Magasin::trouverProduitDansLocation(int idProduit) const{
    for (int i=0; i<_locations.size(); i++){
        if (_locations[i]._idProduit==idProduit){
            return true;
        }
    }
    return false;
}

std::vector<int> Magasin::calculerProduitsLibres() const{
    std::vector<int> _tabProduitsLibres;
    for (int i=0; i<_produits.size(); i++) {
        if (!trouverProduitDansLocation(_produits[i].getId())) {
            _tabProduitsLibres.push_back(_produits[i].getId());
        }
    }
    for (int i=0; i<_tabProduitsLibres.size();i++){
        std::cout<<_tabProduitsLibres[i]<<std::endl;
    }
return _tabProduitsLibres;
}