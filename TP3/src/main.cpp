#include "Location/Location.hpp"
#include "Client/Client.hpp"
#include "Produit/Produit.hpp"
#include "Magasin/Magasin.hpp"

#include <iostream>

int main() {
    Location l1(1,2);
    Client c1(1,"toto");
    Produit p1(1,"table");
    Magasin m1;

    std::cout<<"affichage de la location"<<std::endl;
    l1.afficherLocation();

    std::cout<<std::endl;
    std::cout<<"affichage du client"<<std::endl;
    c1.afficherClient();

    std::cout<<std::endl;
    std::cout<<"affichage du produit"<<std::endl;
    p1.afficherProduit();

    std::cout<<std::endl;
    std::cout<<"affichage des clients"<<std::endl;
    m1.ajouterClient("toto");
    m1.ajouterClient("titi");
    m1.ajouterClient("tata");
    m1.ajouterClient("tutu");
    m1.ajouterClient("toutou");
    m1.afficherClients();
    try{
        m1.supprimerClient(4);
    }catch (std::string e){
        std::cerr << e << std::endl;
    }
    std::cout<<std::endl;
    std::cout<<"affichage des clients apres suppression"<<std::endl;
    m1.afficherClients();

    std::cout<<std::endl;
    std::cout<<"affichage des produits"<<std::endl;
    m1.ajouterProduit("table");
    m1.ajouterProduit("lit");
    m1.ajouterProduit("chaise");
    m1.ajouterProduit("canape");
    m1.ajouterProduit("bureau");
    m1.ajouterProduit("etagere");
    m1.afficherProduits();
    try {
        m1.supprimerProduit(4);
    }catch (std::string e){
        std::cerr << e << std::endl;
    }
    std::cout<<std::endl;
    std::cout<<"affichage des produits apres suppression"<<std::endl;
    m1.afficherProduits();

    m1.ajouterLocation(1, 3);
    m1.ajouterLocation(0,0);
    m1.ajouterLocation(3, 1);
    m1.ajouterLocation(14, 6);
    std::cout<<std::endl;
    std::cout<<"nombre de locations "<<m1.nbLocations()<<std::endl;

    std::cout<<std::endl;
    std::cout<<"contenu de la location"<<std::endl;
    m1.afficherLocation();
    m1.supprimerLocation(14,6);
    std::cout<<"contenu de la location apres suppression"<<std::endl;
    m1.afficherLocation();
    std::cout<<std::endl;
    std::cout<<"nombre de locations apres suppression "<<m1.nbLocations()<<std::endl;
    try{
        m1.supprimerLocation(14,6);
    }catch (std::string e){
        std::cerr << e << std::endl;
    }

    std::cout<<std::endl;
    std::cout<< "liste des clients n'ayant pas de location(s)"<< std::endl;

    m1.calculerClientsLibres();

    std::cout<<std::endl;
    std::cout<< "liste des produits non loues"<< std::endl;

    m1.calculerProduitsLibres();

    return 0;
}

