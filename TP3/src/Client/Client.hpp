//
// Created by ccoquet on 28/02/19.
//

#ifndef TP_VIDE_CLIENT_H
#define TP_VIDE_CLIENT_H

#include <string>

class Client {

private:
    int _id;
    std::string _nom;

public:
    Client(int id, const std::string & nom);
    int getId() const;
    const std::string & getNom() const;
    void afficherClient() const;

};
#endif //TP_VIDE_CLIENT_H
