//
// Created by ccoquet on 28/02/19.
//

#include "Client.hpp"
#include <iostream>

/**constructeur*/
Client::Client(int _id, const std::string &_nom) : _id(_id), _nom(_nom) {}


/**méthodes*/
int Client::getId() const{
    return _id;
}

const std::string & Client::getNom() const{
    return _nom;
}

void Client::afficherClient() const{
    std::cout<<"Client ("<<getId()<<","<<getNom()<<")"<<std::endl;
}