#include "Produit.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupProduit) { };

TEST(GroupProduit, Produit_test1)  {
    Produit p(1, "table");
    CHECK_EQUAL(p.getId(), 1);
}

TEST(GroupProduit, Produit_test2)  {
    Produit p(1, "chaise");
    CHECK_EQUAL(p.getDescription(), "chaise");
}

