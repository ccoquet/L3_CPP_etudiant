//
// Created by ccoquet on 01/03/19.
//

#ifndef TP_VIDE_PRODUIT_H
#define TP_VIDE_PRODUIT_H

#include <string>

class Produit{

private:
    int _id;
    std::string _description;

public:
    Produit(int id, const std::string & description);
    int getId() const;
    const std::string & getDescription() const;
    void afficherProduit() const;

};
#endif //TP_VIDE_PRODUIT_H
