//
// Created by ccoquet on 01/03/19.
//

#include "Produit.hpp"
#include <iostream>

/**constructeur */
Produit::Produit(int id, const std::string & description): _id(id), _description(description){}


/**méthodes */
int Produit::getId() const{
    return _id;
}

const std::string & Produit::getDescription() const{
    return _description;
}

void Produit::afficherProduit() const{
    std::cout<<"Produit ("<<getId()<<","<<getDescription()<<")"<<std::endl;

}


