//
// Created by ccoquet on 28/02/19.
//

#include "Location.hpp"
#include <iostream>

//constructeur
Location::Location (int idC, int idP){
    _idClient = idC;
    _idProduit = idP;
}


void Location::afficherLocation() const {
    std::cout<<"Location ("<<_idClient<<" , "<<_idProduit<<" )"<<std::endl;
}