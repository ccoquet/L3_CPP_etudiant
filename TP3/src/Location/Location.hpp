//
// Created by ccoquet on 28/02/19.
//

#ifndef LOCATION_HPP
#define LOCATION_HPP

struct Location {

    int _idClient;
    int _idProduit;

    //constructeur
    Location (int idC, int idP);

    void afficherLocation() const;
};

#endif // LOCATION_HPP

