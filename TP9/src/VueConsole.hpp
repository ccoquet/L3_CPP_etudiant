//
// Created by ccoquet on 05/04/19.
//

#ifndef TP8_VUECONSOLE_HPP
#define TP8_VUECONSOLE_HPP

#include "Vue.hpp"

class Controleur;

/**
 * classe dérivée de la classe Vue.
 * Permet d'afficher le texte de l'inventaire dans la sortie standard
 */
class VueConsole : public Vue{

public:
    VueConsole(Controleur & controleur);
    void actualiser();
    void run();

};


#endif //TP8_VUECONSOLE_HPP
