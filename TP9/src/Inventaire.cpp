#include <algorithm>
#include <iostream>

#include "Inventaire.hpp"

/**
 * méthode implementant l'operateur de flux de sortie, afin d'écrire le contenu d'un inventaire
 * dans un fichier
 * @param os de type ostream
 * @param i de type Inventaire
 * @return flux de sortie
 */
std::ostream & operator<<(std::ostream & os, const Inventaire & i){
    std::locale vieuxLoc = std::locale::global(std::locale("fr_FR.UTF-8"));
    for(Bouteille b : i._bouteilles ){
        os<<b;
    }
    std::locale::global(vieuxLoc);
    return os;
}

/**
 * méthode implementant l'operateur de flux d'entree, afin de recuperer les donnes d'un fichier
 * et de les placer dans l'inventaire
 * @param is de type istream
 * @param i de type Inventaire
 * @return flux d'entree
 */
std::istream & operator>>(std::istream & is, Inventaire & i){
    std::locale vieuxLoc = std::locale::global(std::locale("fr_FR.UTF-8"));
    Bouteille b;
    is>>b;
    while(is){
        i._bouteilles.push_back(b);
        is>>b;
    }
    std::locale::global(vieuxLoc);
    return is;

}

void Inventaire::trier()  {
    std::sort(this->_bouteilles.begin(),this->_bouteilles.end(), [](const Bouteille& b1, const Bouteille& b2) {
        return b1._nom < b2._nom;
    });
}