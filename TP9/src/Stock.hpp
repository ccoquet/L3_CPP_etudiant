//
// Created by ccoquet on 11/04/19.
//

#ifndef TP9_STOCK_HPP
#define TP9_STOCK_HPP

#include <string>
#include <map>
#include "Inventaire.hpp"

/**
 * class Stock permettant de renseigner sur le volume total de chaque produit
 */
class Stock {
    public:
        std::map <std::string, float> _produits;

    public:
        void recalculerStock( const Inventaire& inventaire);
};

std::ostream & operator<<(std::ostream & os,const Stock & s);

#endif //TP9_STOCK_HPP
