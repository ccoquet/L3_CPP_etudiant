//
// Created by ccoquet on 11/04/19.
//

#include "Stock.hpp"

#include <map>
#include <locale>

void Stock::recalculerStock( const Inventaire& inventaire){
    //je parcours mon inventaire
    for(Bouteille b : inventaire._bouteilles){
        auto it = this->_produits.find(b._nom);
        if(it != this->_produits.end()){
            //j'ajoute la valeur du volume à la bouteille
           this-> _produits[b._nom] += b._volume;
        }else{
            //j'insère la bouteille
           this-> _produits[b._nom] = b._volume;
        }
    }
}

std::ostream & operator<<(std::ostream & os, const Stock & s){
    std::locale vieuxLoc = std::locale::global(std::locale("fr_FR.UTF-8"));
    for (auto it=s._produits.begin(); it!=s._produits.end(); ++it ){
        os << it->first <<","<< it->second <<std::endl;
    }
    std::locale::global(vieuxLoc);
    return os;
}