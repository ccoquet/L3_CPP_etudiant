#include "Controleur.hpp"


#include <cassert>
#include <fstream>
#include <iostream>
#include <sstream>

Controleur::Controleur(int argc, char ** argv) {
    //ajout des vues console et graphique
    _vues.push_back(std::make_unique<VueConsole>( *this));
    _vues.push_back(std::make_unique<VueGraphique>(argc, argv, *this));
    //ajout d'une bouteille de test
    //_inventaire._bouteilles.push_back(Bouteille{"Test","1978-06-07", 0.75f});
    //chargerInventaire("FichierT");
    //for (auto & v : _vues)
    //  v->actualiser();
}

void Controleur::run() {
    for (auto & v : _vues)
        v->run();
}

/**
 * méthode qui retourne le texte correspondant à l'inventaire trié ainsi que le stock
 * @return chaine de caractères
 */
std::string Controleur::getTexte() {
    std::ostringstream oss;
    _inventaire.trier();
    _stock.recalculerStock(_inventaire);
    oss<< _inventaire << _stock;
    return oss.str();
}

/**
 * Méthode qui permet de charger un inventaire à partir d'un fichier de nom donné
 * @param nomFichier
 */
void Controleur::chargerInventaire(const std::string& nomFichier){
    //ajout d'une bouteille test
    //_inventaire._bouteilles.push_back(Bouteille{"Test","1978-06-07", 0.75f}
    //chargement du fichier dans l'inventaire
    std::fstream fs(nomFichier, std::ios::in);
    if(fs.is_open()){
        fs>>_inventaire;
    }
    fs.close();
    //mise à jour des vues
    for (auto & v : _vues)
        v->actualiser();

}
