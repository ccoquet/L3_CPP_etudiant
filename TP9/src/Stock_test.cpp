//
// Created by ccoquet on 17/04/19.
//

#include "Stock.hpp"

#include <sstream>

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupStock) { };

TEST(GroupStock, TestStock_1){
    std::locale vieuxLoc = std::locale::global(std::locale("fr_FR.UTF-8"));
    Stock s;
    Inventaire inventaire;
    inventaire._bouteilles.push_back(Bouteille{"cyanure", "2013-08-18", 0.25});
    inventaire._bouteilles.push_back(Bouteille{"mescaline", "2013-06-18", 0.1});
    inventaire._bouteilles.push_back(Bouteille{"cyanure", "2013-08-18", 1.25});
    inventaire._bouteilles.push_back(Bouteille{"mescaline", "2013-06-18", 1.0});
    s.recalculerStock(inventaire);
    std::ostringstream oss;
    // TODO decommenter
    oss << s;
    CHECK_EQUAL(oss.str(), "cyanure,1,5\nmescaline,1,1\n");
    std::locale::global(vieuxLoc);
}

