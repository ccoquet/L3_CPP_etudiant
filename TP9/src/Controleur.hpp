#ifndef CONTROLEUR_HPP_
#define CONTROLEUR_HPP_

#include "Inventaire.hpp"
#include "Vue.hpp"
#include "VueConsole.hpp"
#include "Stock.hpp"

#include <memory>
#include <vector>

// Controleur : fait le lien entre la Vue et le Modèle. Point d'entrée de
// l'application. Utilisation : construire un Controleur puis run().
class Controleur {
    private:
        Inventaire _inventaire;
        Stock _stock;
        std::vector<std::unique_ptr<Vue>> _vues;

    public:
        Controleur(int argc, char ** argv);

        // Lance l'application.
        void run();

        //methode qui retourne le texte correspondant a l'inventaire
        std::string getTexte();
        //methode qui permet de charger un inventaire a partir d'un fichier donne
        void chargerInventaire(const std::string& nomFichier);

};

#endif
