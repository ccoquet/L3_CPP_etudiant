//
// Created by ccoquet on 14/03/19.
//

#include "PolygoneRegulier.hpp"
#include <iostream>
#include <cmath>

#define PI 3.14159265

PolygoneRegulier::PolygoneRegulier(const Couleur & couleur, const Point & centre, int rayon, int nbCotes):
    FigureGeometrique(couleur), _nbPoints(nbCotes) {
    _points= new Point[nbCotes];
    float angle = (2.f * PI )/ nbCotes;
    for (int i = 0; i < nbCotes; i++) {
        _points[i] = Point(
                static_cast<int>(centre._x +  rayon * cos(angle * i)),
                static_cast<int>(centre._y + rayon * sin(angle* i))
        );
    }
}

void PolygoneRegulier::afficher(Cairo::RefPtr<Cairo::Context>& myContest) const{
    //std::cout<<"PolygoneRegulier "<<getCouleur()._r<<"_"<<getCouleur()._g<<"_"<<getCouleur()._b;
    // règle le tracé
    myContest->set_source_rgb(getCouleur()._r, getCouleur()._g, getCouleur()._b);
    myContest->set_line_width(2.0);
    myContest->move_to(_points[0]._x, _points[0]._y);
    for(int i = 1; i < getNbPoints(); i++){
        //std::cout<<" "<<_points[i]._x<<"_"<<_points[i]._y;
        myContest->line_to(_points[i]._x, _points[i]._y);
    }
    myContest->line_to(_points[0]._x, _points[0]._y);

    // met à jour l'affichage
    myContest->stroke();
    //std::cout<<std::endl;
}

int PolygoneRegulier::getNbPoints() const{
    return _nbPoints;
}

const Point & PolygoneRegulier::getPoint(int indice) const{
    return _points[indice];
}

PolygoneRegulier::~PolygoneRegulier(){
    delete[] _points;
}