//
// Created by ccoquet on 10/03/19.
//

#ifndef TP_5_COULEUR_HPP
#define TP_5_COULEUR_HPP

struct Couleur{

    double _r;
    double _g;
    double _b;

    Couleur(double r, double g, double b): _r(r),_g(g),_b(b){};

};
#endif //TP_5_COULEUR_HPP
