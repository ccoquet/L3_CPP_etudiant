//
// Created by ccoquet on 19/03/19.
//

#ifndef TP_5_ZONEDESSIN_HPP
#define TP_5_ZONEDESSIN_HPP


#include "../FigureGeometrique/FigureGeometrique.hpp"
#include<vector>
#include <gdk/gdkevents.h>
#include <gtkmm/drawingarea.h>

class ZoneDessin : public Gtk::DrawingArea{

private:
    std::vector<FigureGeometrique*> _figures;

public:
    ZoneDessin();
    ~ZoneDessin();

protected:
    bool on_expose_event(GdkEventExpose* event);
    bool gererClic(GdkEventButton* event);
};


#endif //TP_5_ZONEDESSIN_HPP
