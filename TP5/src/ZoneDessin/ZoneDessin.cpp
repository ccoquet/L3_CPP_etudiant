//
// Created by ccoquet on 19/03/19.
//

#include "ZoneDessin.hpp"
#include "../PolygoneRegulier/PolygoneRegulier.hpp"
#include "../Ligne/Ligne.hpp"


#include<cstdlib>


ZoneDessin::ZoneDessin() {
    Couleur c(1.0, 0.0, 0.0);
    Point p0(50, 50);
    Point p1(100, 200);
    int rayon = 50;
    int nbCotes = 5;

    PolygoneRegulier *polR = new PolygoneRegulier(c, p1, rayon, nbCotes);
    PolygoneRegulier *polR1 = new PolygoneRegulier(c, p0, rayon, nbCotes);
    Ligne *l1 = new Ligne(c, p0, p1);

    _figures.push_back(polR);
    _figures.push_back(polR1);
    _figures.push_back(l1);


    add_events(Gdk::BUTTON_PRESS_MASK); //écoute des événements liés à la souris
    //connection de l'événement à l'objet
    this->signal_button_press_event().connect(sigc::mem_fun(*this, &ZoneDessin::gererClic));

}

ZoneDessin::~ZoneDessin(){
    for (FigureGeometrique * ptrFigGeom : _figures){
        delete ptrFigGeom;
    }
}


bool ZoneDessin::on_expose_event(GdkEventExpose* event){
    Cairo::RefPtr<Cairo::Context> myContext = get_window()->create_cairo_context();
    for (FigureGeometrique * ptrFigGeom : _figures){
        ptrFigGeom->afficher(myContext);
    }
    return true;
}

bool ZoneDessin::gererClic(GdkEventButton* event){
    float Rc = rand();
    float Rg = rand();
    float Rb =rand();
    float couleurR = (Rc/RAND_MAX);
    float couleurG = (Rg/RAND_MAX);
    float couleurB = (Rb/RAND_MAX);
    float nb1 =rand();
    float nb2 =rand();
    int minCotes = 4;
    int maxCotes = 30;
    int nbCotes = ((nb1/RAND_MAX) * (maxCotes - minCotes)) + minCotes;
    int minRayon = 10;
    int maxRayon = 50;
    int rayon=((nb2/RAND_MAX) * (maxRayon - minRayon)) + minRayon;
    if (event->button==1){
        Couleur c(couleurR, couleurG, couleurB);
        Point p (event->x, event->y);
        PolygoneRegulier * polReg = new PolygoneRegulier(c,p,rayon,nbCotes);
        _figures.push_back(polReg);
    }

    if(event->button==3 and not _figures.empty()){
        delete _figures.back();
        _figures.pop_back();
    }
    queue_draw();
    return true;
}