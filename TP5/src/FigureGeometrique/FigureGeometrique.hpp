//
// Created by ccoquet on 10/03/19.
//

#ifndef TP_5_FIGUREGEOMETRIQUE_HPP
#define TP_5_FIGUREGEOMETRIQUE_HPP


#include <cairomm/context.h>
#include "../Couleur.hpp"

class FigureGeometrique {

private:
    Couleur _couleur;

public:
    FigureGeometrique(const Couleur & couleur);
    const Couleur & getCouleur() const;
    virtual void afficher(Cairo::RefPtr<Cairo::Context>& myContest) const =0;
    virtual ~FigureGeometrique();

};


#endif //TP_5_FIGUREGEOMETRIQUE_HPP
