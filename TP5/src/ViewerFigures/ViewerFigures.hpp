//
// Created by ccoquet on 19/03/19.
//

#ifndef TP_5_VIEWERFIGURES_HPP
#define TP_5_VIEWERFIGURES_HPP


#include <gtkmm.h>
#include "../ZoneDessin/ZoneDessin.hpp"

class ViewerFigures {

private:
        Gtk::Main _kit;
        Gtk::Window _window;
        ZoneDessin zD;

public:
    ViewerFigures(int argc, char** argv);
    void run();
};


#endif //TP_5_VIEWERFIGURES_HPP
