//
// Created by ccoquet on 19/03/19.
//

#include "ViewerFigures/ViewerFigures.hpp"
#include "ZoneDessin/ZoneDessin.hpp"
#include <gtkmm.h>

int main(int argc, char ** argv) {

   ViewerFigures v(argc, argv);
   v.run();


}