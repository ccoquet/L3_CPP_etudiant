//
// Created by ccoquet on 13/03/19.
//

#include "Ligne.hpp"
#include <iostream>
#include <cairomm/context.h>


//constructeur faisant appel au constructeur de la classe mère "FigureGeometrique"
Ligne::Ligne (const Couleur & couleur, const Point & p0, const Point & p1): FigureGeometrique(couleur), _p0(p0), _p1(p1){}


void Ligne::afficher( Cairo::RefPtr<Cairo::Context>& myContest) const{
    /**std::cout<<"Ligne "<<getCouleur()._r<<"_"<<getCouleur()._g<<"_"<<getCouleur()._b<<" "<<
            getP0()._x<<"_"<<getP0()._y<<" "<<getP1()._x<<"_"<<getP1()._y<<std::endl;*/
    // règle le tracé
    myContest->set_source_rgb(getCouleur()._r, getCouleur()._g, getCouleur()._b);
    myContest->set_line_width(5.0);
    // dessine une ligne
    myContest->move_to(getP0()._x, getP0()._y);
    myContest->line_to(getP1()._x, getP1()._y);

    // met à jour l'affichage
    myContest->stroke();
}

const Point & Ligne::getP0() const{
    return  _p0;
}

const Point & Ligne::getP1() const{
    return _p1;
}