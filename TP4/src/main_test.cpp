//
// Created by ccoquet on 13/03/19.
//

#include <CppUTest/CommandLineTestRunner.h>

int main(int argc, char ** argv) {
    return CommandLineTestRunner::RunAllTests(argc, argv);
}