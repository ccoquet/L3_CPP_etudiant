//
// Created by ccoquet on 13/03/19.
//

#include "Ligne.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupLigne) { };

TEST(GroupLigne, Ligne_test1)  {
    Couleur c (1.0, 0.0, 0.0);
    Point p0(0,0);
    Point p1 (100,200);
    Ligne l (c,p0,p1);
    CHECK_EQUAL(l.getP0()._x, 0);
    CHECK_EQUAL(l.getP1()._x, 100);
    CHECK_EQUAL(l.getP0()._y, 0);
    CHECK_EQUAL(l.getP1()._y, 200);
}