//
// Created by ccoquet on 13/03/19.
//

#ifndef TP_4_LIGNE_HPP
#define TP_4_LIGNE_HPP


#include "../FigureGeometrique/FigureGeometrique.hpp"
#include "../Point.hpp"

class Ligne : public FigureGeometrique {

private:
    Point _p0;
    Point _p1;

public:
    Ligne(const Couleur & couleur, const Point & p0, const Point & p1);
    void afficher() const;
    const Point & getP0() const;
    const Point & getP1() const;
};


#endif //TP_4_LIGNE_HPP
