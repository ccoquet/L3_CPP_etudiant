//
// Created by ccoquet on 10/03/19.
//

#include "FigureGeometrique/FigureGeometrique.hpp"
#include "Ligne/Ligne.hpp"
#include "PolygoneRegulier/PolygoneRegulier.hpp"
#include <iostream>
#include <vector>

int main(){
    Couleur c (1.0, 0.0, 0.0);
    Point p0(0,0);
    Point p1 (100,200);
    int rayon=50;
    int nbCotes=5;
    int indice =2;
    Ligne l (c,p0,p1);
    PolygoneRegulier polReg(c, p1, rayon, nbCotes );
    std::cout<<"Ligne"<<std::endl;
    l.afficher();
    std::cout<<std::endl;
    std::cout<<"Polygone Regulier"<<std::endl;
    polReg.afficher();
    std::cout<<"nb de points :"<<polReg.getNbPoints()<<std::endl;
    std::cout<<"point x indice :"<<polReg.getPoint(indice)._x<<std::endl;
    std::cout<<"point y indice :"<<polReg.getPoint(indice)._y<<std::endl;

    std::vector <FigureGeometrique *> figGeom{new Ligne(c,p0,p1), new PolygoneRegulier(c, p1, rayon, nbCotes )};
    std::cout<<std::endl;
    std::cout<<"Couleur des figures geometriques"<<std::endl;

    for (FigureGeometrique * ptrFigGeom : figGeom){
        std::cout<<ptrFigGeom->getCouleur()._r <<" "<<ptrFigGeom->getCouleur()._g<<" "<<
            ptrFigGeom->getCouleur()._b<<std::endl;
    }

    std::cout<<std::endl;
    std::cout<<"Affichage des figures geometriques"<<std::endl;
    for (FigureGeometrique * ptrFigGeom : figGeom){
        ptrFigGeom->afficher();
    }

    for (FigureGeometrique * ptrFigGeom : figGeom){
        delete ptrFigGeom;
    }

    return 0;
}