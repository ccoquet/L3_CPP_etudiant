//
// Created by ccoquet on 15/03/19.
//
#include "PolygoneRegulier.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupPolygoneRegulier) { };

TEST(GroupPolygoneRegulier, PolygoneRegulier_test1)  {
    Couleur c (1.0, 0.0, 0.0);
    Point p1 (100,200);
    int rayon=50;
    int nbCotes=5;
    int indice=2;
    PolygoneRegulier polReg(c, p1, rayon, nbCotes );
    CHECK_EQUAL(polReg.getNbPoints(), 5);
    CHECK_EQUAL(polReg.getPoint(indice)._x,59);
    CHECK_EQUAL(polReg.getPoint(indice)._y,229);    
}