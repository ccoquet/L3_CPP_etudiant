//
// Created by ccoquet on 14/03/19.
//

#include "PolygoneRegulier.hpp"
#include <iostream>
#include <cmath>

#define PI 3.14159265

PolygoneRegulier::PolygoneRegulier(const Couleur & couleur, const Point & centre, int rayon, int nbCotes):
    FigureGeometrique(couleur), _nbPoints(nbCotes) {
    _points= new Point[nbCotes];
    float angle = (2.f * PI )/ nbCotes;
    for (int i = 0; i < nbCotes; i++) {
        _points[i] = Point(
                static_cast<int>(centre._x +  rayon * cos(angle * i)),
                static_cast<int>(centre._y + rayon * sin(angle* i))
        );
    }
}

void PolygoneRegulier::afficher() const{
    std::cout<<"PolygoneRegulier "<<getCouleur()._r<<"_"<<getCouleur()._g<<"_"<<getCouleur()._b;
    for(int i = 0; i < getNbPoints(); i++){
        std::cout<<" "<<_points[i]._x<<"_"<<_points[i]._y;
    }
    std::cout<<std::endl;
}

int PolygoneRegulier::getNbPoints() const{
    return _nbPoints;
}

const Point & PolygoneRegulier::getPoint(int indice) const{
    return _points[indice];
}

PolygoneRegulier::~PolygoneRegulier(){
    delete[] _points;
}