//
// Created by ccoquet on 14/03/19.
//

#ifndef TP_4_POLYGONEREGULIER_HPP
#define TP_4_POLYGONEREGULIER_HPP


#include "../Point.hpp"
#include "../FigureGeometrique/FigureGeometrique.hpp"

class PolygoneRegulier : public FigureGeometrique {

private:
    int _nbPoints;
    Point * _points;

public:
    PolygoneRegulier(const Couleur & couleur, const Point & centre, int rayon, int nbCotes);
    void afficher() const;
    int getNbPoints() const;
    const Point & getPoint(int indice) const;
    ~PolygoneRegulier();

};


#endif //TP_4_POLYGONEREGULIER_HPP
