//
// Created by ccoquet on 10/03/19.
//

#include "FigureGeometrique.hpp"

//constructeur

FigureGeometrique::FigureGeometrique(const Couleur & couleur): _couleur(couleur){}

//méthodes

const Couleur & FigureGeometrique::getCouleur() const{
    return _couleur;
}

FigureGeometrique::~FigureGeometrique() {}