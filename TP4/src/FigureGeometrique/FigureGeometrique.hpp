//
// Created by ccoquet on 10/03/19.
//

#ifndef TP_4_FIGUREGEOMETRIQUE_HPP
#define TP_4_FIGUREGEOMETRIQUE_HPP


#include "../Couleur.hpp"

class FigureGeometrique {

private:
    Couleur _couleur;

public:
    FigureGeometrique(const Couleur & couleur);
    const Couleur & getCouleur() const;
    virtual void afficher() const =0;
    virtual ~FigureGeometrique();

};


#endif //TP_4_FIGUREGEOMETRIQUE_HPP
