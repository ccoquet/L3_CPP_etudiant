//
// Created by ccoquet on 10/03/19.
//

#ifndef TP_4_POINT_HPP
#define TP_4_POINT_HPP
struct Point{
    int _x;
    int _y;

    Point() {};
    Point(int x, int y): _x(x),_y(y){};

};
#endif //TP_4_POINT_HPP
